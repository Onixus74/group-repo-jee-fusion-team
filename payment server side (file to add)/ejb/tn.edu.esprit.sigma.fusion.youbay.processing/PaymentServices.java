package tn.edu.esprit.sigma.fusion.youbay.processing;

import javax.ejb.Stateless;

import net.authorize.sample.PaymentTransactions.ChargeCreditCard;

/**
 * Session Bean implementation class PaymentServices
 */
@Stateless
public class PaymentServices implements PaymentServicesRemote {
	// These are default transaction keys.
    // You can create your own keys in seconds by signing up for a sandbox account here: https://developer.authorize.net/sandbox/
    String apiLoginId           = "9Bvezt7X4ra";
    String transactionKey       = "8BbwQ7Mt2rA23M26";
    //Update the payedId with which you want to run the sample code
    String payerId 				= "10";
    //Update the transactionId with which you want to run the sample code
    String transactionId 		= "2241801682";
    /**
     * Default constructor. 
     */
    public PaymentServices() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void chargeCreditCard() {
		System.out.println("PaymentServices");
		ChargeCreditCard.run(apiLoginId, transactionKey);
	}

}
