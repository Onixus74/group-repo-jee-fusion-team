package tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate;

import java.util.List;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.AssistantItems;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Auction;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Category;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.CustomizedAds;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViews;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViewsId;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Manager;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReviewId;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ProductHistory;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.SpecialPromotion;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.AssistantItemsServicesRemote;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.AuctionServicesRemote;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.BuyerServicesRemote;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.CategoryServicesRemote;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.CustomizedAdsServicesRemote;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.HistoryOfViewsServicesRemote;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.ManagerServicesRemote;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.OrderAndReviewServicesRemote;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.ProductHistoryServicesRemote;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.ProductServicesRemote;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.SellerServicesRemote;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.SpecialPromotionServicesRemote;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.SubcategoryServicesRemote;
import tn.edu.esprit.sigma.fusion.youbayclient.locator.ServiceLocator;

public class BasicFonctionnalitiesDelegate {
	/*
	 * Assistant Items Services Remote
	 */
	public final static String moduleName="youbay-war";	
	
	private static final String jndiNameAssistantItemsServicesRemote = moduleName+"/AssistantItemsServices!tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.AssistantItemsServicesRemote";

	private static AssistantItemsServicesRemote assistantItemsServicesGetProxy() {
		return (AssistantItemsServicesRemote) ServiceLocator.getInstance()
				.getProxy(jndiNameAssistantItemsServicesRemote);
	}

	public static Boolean doAddAssistantItems(AssistantItems theAssistantItems) {

		return assistantItemsServicesGetProxy().addAssistantItems(
				theAssistantItems);
	}

	public static Long doAddAssistantItemsAndReturnId(AssistantItems theAssistantItems) {

		return assistantItemsServicesGetProxy().addAssistantItemsAndReturnId(theAssistantItems);
	}

	public static AssistantItems doFindAssistantItemsById(Long theId) {

		return assistantItemsServicesGetProxy().findAssistantItemsById(theId);
	}

	public static Boolean doUpdateAssistantItems(
			AssistantItems theAssistantItems) {

		return assistantItemsServicesGetProxy().updateAssistantItems(
				theAssistantItems);
	}

	public static Boolean doDeleteAssistantItems(
			AssistantItems theAssistantItems) {

		return assistantItemsServicesGetProxy().deleteAssistantItems(
				theAssistantItems);
	}

	public static Boolean doDeleteAssistantItemsById(Long theId) {

		return assistantItemsServicesGetProxy().deleteAssistantItemsById(theId);
	}

	public static List<AssistantItems> doFindAllAssistantItems() {
		return assistantItemsServicesGetProxy().findAllAssistantItems();
	}

	/*
	 * Auction Services Delegate
	 */

	private static final String jndiNameAuctionServicesRemote = moduleName+"/AuctionServices!tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.AuctionServicesRemote";

	private static AuctionServicesRemote auctionServicesGetProxy() {
		return (AuctionServicesRemote) ServiceLocator.getInstance().getProxy(
				jndiNameAuctionServicesRemote);
	}

	public static Boolean doAddAuction(Auction theAuction) {

		return auctionServicesGetProxy().addAuction(theAuction);
	}

	public static Long doAddAuctionAndReturnId(Auction theAuction) {

		return auctionServicesGetProxy().addAuctionAndReturnId(theAuction);
	}

	public static Auction doFindAuctionById(Long theId) {

		return auctionServicesGetProxy().findAuctionById(theId);
	}

	public static Boolean doUpdateAuction(Auction theAuction) {

		return auctionServicesGetProxy().updateAuction(theAuction);
	}

	public static Boolean doDeleteAuction(Auction theAuction) {

		return auctionServicesGetProxy().deleteAuction(theAuction);
	}

	public static Boolean doDeleteAuctionById(Long theId) {

		return auctionServicesGetProxy().deleteAuctionById(theId);
	}

	public static List<Auction> doFindAllAuction() {
		return auctionServicesGetProxy().findAllAuction();
	}

	/*
	 * BuyerServicesDelegate
	 */

	private static final String jndiNameBuyerServicesRemote = moduleName+"/BuyerServices!tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.BuyerServicesRemote";

	private static BuyerServicesRemote buyerServicesGetProxy() {
		return (BuyerServicesRemote) ServiceLocator.getInstance().getProxy(
				jndiNameBuyerServicesRemote);
	}

	public static Boolean doAddBuyer(Buyer theBuyer) {

		return buyerServicesGetProxy().addBuyer(theBuyer);
	}

	public static Long doAddBuyerAndReturnId(Buyer theBuyer) {

		return buyerServicesGetProxy().addBuyerAndReturnId(theBuyer);
	}

	public static Buyer doFindBuyerById(Long theId) {

		return buyerServicesGetProxy().findBuyerById(theId);
	}

	public static Boolean doUpdateBuyer(Buyer theBuyer) {

		return buyerServicesGetProxy().updateBuyer(theBuyer);
	}

	public static Boolean doDeleteBuyer(Buyer theBuyer) {

		return buyerServicesGetProxy().deleteBuyer(theBuyer);
	}

	public static Boolean doDeleteBuyerById(Long theId) {

		return buyerServicesGetProxy().deleteBuyerById(theId);
	}

	public static List<Buyer> doFindAllBuyer() {
		return buyerServicesGetProxy().findAllBuyer();
	}

	/*
	 * Category Services Delegate
	 */
	private static final String jndiNameCategoryServicesRemote = moduleName+"/CategoryServices!tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.CategoryServicesRemote";

	private static CategoryServicesRemote categoryServicesGetProxy() {
		return (CategoryServicesRemote) ServiceLocator.getInstance().getProxy(
				jndiNameCategoryServicesRemote);
	}

	public static Boolean doAddCategory(Category theCategory) {

		return categoryServicesGetProxy().addCategory(theCategory);
	}


	public static Long doAddCategoryAndReturnId(Category theCategory) {

		return categoryServicesGetProxy().addCategoryAndReturnId(theCategory);
	}

	public static Category doFindCategoryById(Long theId) {

		return categoryServicesGetProxy().findCategoryById(theId);
	}

	public static Boolean doUpdateCategory(Category theCategory) {

		return categoryServicesGetProxy().updateCategory(theCategory);
	}

	public static Boolean doDeleteCategory(Category theCategory) {

		return categoryServicesGetProxy().deleteCategory(theCategory);
	}

	public static Boolean doDeleteCategoryById(Long theId) {

		return categoryServicesGetProxy().deleteCategoryById(theId);
	}

	public static List<Category> doFindAllCategory() {

		return categoryServicesGetProxy().findAllCategory();
	}

	/*
	 * Customized Ads Services Delegate
	 */
	private static final String jndiNameCustomizedAdsServicesRemote = moduleName+"/CustomizedAdsServices!tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.CustomizedAdsServicesRemote";

	private static CustomizedAdsServicesRemote customizedAdsServicesGetProxy() {
		return (CustomizedAdsServicesRemote) ServiceLocator.getInstance()
				.getProxy(jndiNameCustomizedAdsServicesRemote);
	}

	public static Boolean doAddCustomizedAds(CustomizedAds theCustomizedAds) {

		return customizedAdsServicesGetProxy().addCustomizedAds(
				theCustomizedAds);
	}

	public static Long doAddCustomizedAdsAndReturnId(CustomizedAds theCustomizedAds) {

		return customizedAdsServicesGetProxy().addCustomizedAdsAndReturnId(theCustomizedAds);	}

	public static CustomizedAds doFindCustomizedAdsById(Long theId) {

		return customizedAdsServicesGetProxy().findCustomizedAdsById(theId);
	}

	public static Boolean doUpdateCustomizedAds(CustomizedAds theCustomizedAds) {

		return customizedAdsServicesGetProxy().updateCustomizedAds(
				theCustomizedAds);
	}

	public static Boolean doDeleteCustomizedAds(CustomizedAds theCustomizedAds) {

		return customizedAdsServicesGetProxy().deleteCustomizedAds(
				theCustomizedAds);
	}

	public static Boolean doDeleteCustomizedAdsById(Long theId) {

		return customizedAdsServicesGetProxy().deleteCustomizedAdsById(theId);
	}

	public static List<CustomizedAds> doFindAllCustomizedAds() {
		return customizedAdsServicesGetProxy().findAllCustomizedAds();

	}

	/*
	 * HistoryOfViewsServices
	 */
	private static final String jndiNameHistoryOfViewsServices = moduleName+"/HistoryOfViewsServices!tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.HistoryOfViewsServicesRemote";

	private static HistoryOfViewsServicesRemote historyOfViewsServicesGetProxy() {
		return (HistoryOfViewsServicesRemote) ServiceLocator.getInstance()
				.getProxy(jndiNameHistoryOfViewsServices);
	}

	public static Boolean doAddHistoryOfViews(HistoryOfViews theHistoryOfViews) {
		return historyOfViewsServicesGetProxy().addHistoryOfViews(
				theHistoryOfViews);
	}	


	public static HistoryOfViews doFindHistoryOfViewsById(
			HistoryOfViewsId theHistoryOfViewsIdClass) {
		return historyOfViewsServicesGetProxy().findHistoryOfViewsById(
				theHistoryOfViewsIdClass);
	}

	public static Boolean doUpdateHistoryOfViews(
			HistoryOfViews theHistoryOfViews) {
		return historyOfViewsServicesGetProxy().updateHistoryOfViews(
				theHistoryOfViews);
	}

	public static Boolean doDeleteHistoryOfViewsById(
			HistoryOfViewsId theHistoryOfViewsIdClass) {
		return historyOfViewsServicesGetProxy().deleteHistoryOfViewsById(
				theHistoryOfViewsIdClass);
	}

	public static List<HistoryOfViews> doFindAllHistoryOfViews() {
		return historyOfViewsServicesGetProxy().findAllHistoryOfViews();
	}

	/*
	 * manager services delegate
	 */
	private static final String jndiNameManagerServicesRemote = moduleName+"/ManagerServices!tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.ManagerServicesRemote";

	private static ManagerServicesRemote managerServicesGetProxy() {
		return (ManagerServicesRemote) ServiceLocator.getInstance().getProxy(
				jndiNameManagerServicesRemote);
	}

	public static Boolean doAddManager(Manager theManager) {
		return managerServicesGetProxy().addManager(theManager);
	}


	public static Long doAddManagerAndReturnId(Manager theManager) {
		return managerServicesGetProxy().addManagerAndReturnId(theManager);
	}

	public static Manager doFindManagerById(Long theId) {
		return managerServicesGetProxy().findManagerById(theId);
	}

	public static Boolean doUpdateManager(Manager theManager) {
		return managerServicesGetProxy().updateManager(theManager);
	}

	public static Boolean doDeleteManager(Manager theManager) {
		return managerServicesGetProxy().deleteManager(theManager);
	}

	public static Boolean doDeleteManagerById(Long theId) {
		return managerServicesGetProxy().deleteManagerById(theId);
	}

	public static List<Manager> doFindAllManager() {

		return managerServicesGetProxy().findAllManager();
	}

	/*
	 * Order and Review
	 */
	private static final String jndiNameOrderAndReviewServices = moduleName+"/OrderAndReviewServices!tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.OrderAndReviewServicesRemote";

	private static OrderAndReviewServicesRemote OrderAndReviewServicesGetProxy() {
		return (OrderAndReviewServicesRemote) ServiceLocator.getInstance()
				.getProxy(jndiNameOrderAndReviewServices);
	}

	public static Boolean doAddOrderAndReview(OrderAndReview theOrderAndReview) {
		return OrderAndReviewServicesGetProxy().addOrderAndReview(
				theOrderAndReview);
	}

	public static OrderAndReview doFindOrderAndReviewById(
			OrderAndReviewId theOrderAndReviewIdClass) {
		return OrderAndReviewServicesGetProxy().findOrderAndReviewById(
				theOrderAndReviewIdClass);
	}

	public static Boolean doUpdateOrderAndReview(
			OrderAndReview theOrderAndReview) {
		return OrderAndReviewServicesGetProxy().updateOrderAndReview(
				theOrderAndReview);
	}

	public static Boolean doDeleteOrderAndReviewById(
			OrderAndReviewId theOrderAndReviewIdClass) {
		return OrderAndReviewServicesGetProxy().deleteOrderAndReviewById(
				theOrderAndReviewIdClass);
	}

	public static List<OrderAndReview> doFindAllOrderAndReview() {
		return OrderAndReviewServicesGetProxy().findAllOrderAndReview();
	}

	/*
	 * ProductHistoryServicesDelegate
	 */

	private static final String jndiNameProductHistoryServices = moduleName+"/ProductHistoryServices!tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.ProductHistoryServicesRemote";

	private static ProductHistoryServicesRemote ProductHistoryServicesGetProxy() {
		return (ProductHistoryServicesRemote) ServiceLocator.getInstance()
				.getProxy(jndiNameProductHistoryServices);
	}

	public static Boolean doAddProductHistory(ProductHistory theProductHistory) {

		return ProductHistoryServicesGetProxy().addProductHistory(
				theProductHistory);
	}

	public static Long doAddProductHistoryAndReturnId(ProductHistory theProductHistory) {

		return ProductHistoryServicesGetProxy().addProductHistoryAndReturnId(theProductHistory);
	}

	
	public static ProductHistory doFindProductHistoryById(Long theId) {

		return ProductHistoryServicesGetProxy().findProductHistoryById(theId);
	}

	public static Boolean doUpdateProductHistory(
			ProductHistory theProductHistory) {

		return ProductHistoryServicesGetProxy().updateProductHistory(
				theProductHistory);
	}

	public static Boolean doDeleteProductHistory(
			ProductHistory theProductHistory) {

		return ProductHistoryServicesGetProxy().deleteProductHistory(
				theProductHistory);
	}

	public static Boolean doDeleteProductHistoryById(Long theId) {

		return ProductHistoryServicesGetProxy().deleteProductHistoryById(theId);
	}

	public static List<ProductHistory> doFindAllProductHistory() {
		return ProductHistoryServicesGetProxy().findAllProductHistory();
	}

	/*
	 * product services delegate
	 */

	private static final String jndiNameProductServices = moduleName+"/ProductServices!tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.ProductServicesRemote";

	private static ProductServicesRemote productServicesGetProxy() {
		return (ProductServicesRemote) ServiceLocator.getInstance().getProxy(
				jndiNameProductServices);
	}

	public static Boolean doAddProduct(Product theProduct) {

		return productServicesGetProxy().addProduct(theProduct);
	}
	public static Long doAddProductAndReturnId(Product theProduct) {

		return productServicesGetProxy().addProductAndReturnId(theProduct);
	}

	public static Product doFindProductById(Long theId) {

		return productServicesGetProxy().findProductById(theId);
	}

	public static Boolean doUpdateProduct(Product theProduct) {

		return productServicesGetProxy().updateProduct(theProduct);
	}

	public static Boolean doDeleteProduct(Product theProduct) {

		return productServicesGetProxy().deleteProduct(theProduct);
	}

	public static Boolean doDeleteProductById(Long theId) {

		return productServicesGetProxy().deleteProductById(theId);
	}

	public static List<Product> doFindAllProduct() {
		return productServicesGetProxy().findAllProduct();
	}

	/*
	 * Seller Services Delegate
	 */

	private static final String jndiNameSellerServices = moduleName+"/SellerServices!tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.SellerServicesRemote";

	private static SellerServicesRemote sellerServicesGetProxy() {
		return (SellerServicesRemote) ServiceLocator.getInstance().getProxy(
				jndiNameSellerServices);
	}

	public static Boolean doAddSeller(Seller theSeller) {
		return sellerServicesGetProxy().addSeller(theSeller);
	}
	public static Long doAddSellerAndReturnId(Seller theSeller) {
		return sellerServicesGetProxy().addSellerAndReturnId(theSeller);
	}

	public static Seller doFindSellerById(Long theId) {
		return sellerServicesGetProxy().findSellerById(theId);
	}

	public static Boolean doUpdateSeller(Seller theSeller) {
		return sellerServicesGetProxy().updateSeller(theSeller);
	}

	public static Boolean doDeleteSeller(Seller theSeller) {
		return sellerServicesGetProxy().deleteSeller(theSeller);
	}

	public static Boolean doDeleteSellerById(Long theId) {
		return sellerServicesGetProxy().deleteSellerById(theId);
	}

	public static List<Seller> doFindAllSeller() {

		return sellerServicesGetProxy().findAllSeller();
	}

	/*
	 * Special Promotion Services Delegate
	 */
	private static final String jndiNameSpecialPromotionServices = moduleName+"/SpecialPromotionServices!tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.SpecialPromotionServicesRemote";

	private static SpecialPromotionServicesRemote specialPromotionGetProxy() {
		return (SpecialPromotionServicesRemote) ServiceLocator.getInstance()
				.getProxy(jndiNameSpecialPromotionServices);
	}

	public static Boolean doAddSpecialPromotion(
			SpecialPromotion theSpecialPromotion) {

		return specialPromotionGetProxy().addSpecialPromotion(
				theSpecialPromotion);
	}
	public static Long doAddSpecialPromotionAndReturnId(
			SpecialPromotion theSpecialPromotion) {

		return specialPromotionGetProxy().addSpecialPromotionAndReturnId(
				theSpecialPromotion);
	}

	public static SpecialPromotion doFindSpecialPromotionById(Long theId) {

		return specialPromotionGetProxy().findSpecialPromotionById(theId);
	}

	public static Boolean doUpdateSpecialPromotion(
			SpecialPromotion theSpecialPromotion) {

		return specialPromotionGetProxy().updateSpecialPromotion(
				theSpecialPromotion);
	}

	public static Boolean doDeleteSpecialPromotion(
			SpecialPromotion theSpecialPromotion) {

		return specialPromotionGetProxy().deleteSpecialPromotion(
				theSpecialPromotion);
	}

	public static Boolean doDeleteSpecialPromotionById(Long theId) {

		return specialPromotionGetProxy().deleteSpecialPromotionById(theId);
	}

	public static List<SpecialPromotion> doFindAllSpecialPromotion() {

		return specialPromotionGetProxy().findAllSpecialPromotion();
	}

	/*
	 * Sub category Services Delegate
	 */
	private static final String jndiNameSubcategoryServices = moduleName+"/SubcategoryServices!tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.SubcategoryServicesRemote";

	private static SubcategoryServicesRemote subcategoryServicesGetProxy() {
		return (SubcategoryServicesRemote) ServiceLocator.getInstance()
				.getProxy(jndiNameSubcategoryServices);
	}

	public static Boolean doAddSubcategory(Subcategory theSubcategory) {
		return subcategoryServicesGetProxy().addSubcategory(theSubcategory);
	}
	public static Long doAddSubcategoryAndReturnId(Subcategory theSubcategory) {
		return subcategoryServicesGetProxy().addSubcategoryAndReturnId(theSubcategory);
	}

	public static Subcategory doFindSubcategoryById(Long theId) {
		return subcategoryServicesGetProxy().findSubcategoryById(theId);
	}

	public static Boolean doUpdateSubcategory(Subcategory theSubcategory) {
		return subcategoryServicesGetProxy().updateSubcategory(theSubcategory);
	}

	public static Boolean doDeleteSubcategory(Subcategory theSubcategory) {

		return subcategoryServicesGetProxy().deleteSubcategory(theSubcategory);
	}

	public static Boolean doDeleteSubcategoryById(Long theId) {

		return subcategoryServicesGetProxy().deleteSubcategoryById(theId);
	}

	public static List<Subcategory> doFindAllSubcategory() {

		return subcategoryServicesGetProxy().findAllSubcategory();
	}

}
