/*
 * 
 * Author Marwen
 */

package tn.edu.esprit.sigma.fusion.youbayclient.processing.delegate;


import java.util.List;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;
import tn.edu.esprit.sigma.fusion.youbay.processing.BuyerBusinessServicesRemote;
import tn.edu.esprit.sigma.fusion.youbayclient.locator.ServiceLocator;

public class BuyerBusinessServicesDelegate {
	private static final String jndiNameBuyerBusinessServicesRemote = "youbay-war/BuyerBusinessServices!tn.edu.esprit.sigma.fusion.youbay.processing.BuyerBusinessServicesRemote";
	
	private static BuyerBusinessServicesRemote BuyerBusinessServicesGetProxy() {
		return (BuyerBusinessServicesRemote) ServiceLocator.getInstance().getProxy(
				jndiNameBuyerBusinessServicesRemote);
	}
	
	public static void doAddProductToCart(Buyer buyer , Product product) {

		BuyerBusinessServicesGetProxy().AddProductToCart(buyer, product);
	}
	
	public static void doConfirmOrder(Buyer buyer , OrderAndReview order) {

		BuyerBusinessServicesGetProxy().ConfirmOrder(buyer, order);
	}
	
	public static String doCompareProducts(Product product1 , Product product2) {

		return BuyerBusinessServicesGetProxy().CompareProducts(product1, product2);
	}
	
	public static List<Product> doFindProductsAdvanced(Float highestPrice, Float lowestPrice, Subcategory subcategory,
			String subCategoryAttributes){
		
		return BuyerBusinessServicesGetProxy().FindProductsAdvanced(highestPrice, lowestPrice, subcategory, subCategoryAttributes);
	}
}
