package tn.edu.esprit.sigma.fusion.youbayclient.processingtest;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.BasicFonctionnalitiesDelegate;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.InitDatabaseServicesDelegate;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.ProcessingDelegate;

public class ProcessingDelegateTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		InitDatabaseServicesDelegate.doTruncateAllTables();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDoSendEmail() {
		System.out.println(new Date());

		ProcessingDelegate.doSendEmail("perso@latiri.com", "haha fassa5ni ", "sa77a yassine spam :p ");
		// System.out.println(">>>>>>>>>>>>>>>>>>>> CLIENT ; IL EST ; "+new
		// Date());
	}

	@Test
	public void testDoChargeCreditCard() {
		ProcessingDelegate.doChargeCreditCard();
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testDoChargeCreditCardForBuyer() {
		Buyer buyer1 = new Buyer("Houssem ", "Sabbegh ", "sabbegh@gmail.com", "21623215434", new Date("17/05/1991"),
				"Tunisia", true, false, "", "1 Purchase;", true, 0f, "18 rue ESPRIT", "", "Ghazela", 0f, 15f);

		Buyer buyer2 = new Buyer("Yassine", "Latiri", "pro@latiri.com", "21623165189", new Date("17/05/1991"),
				"Tunisia", true, false, "", "5 Purchases;", true, 0f, "18 rue ESPRIT", "", "Ghazela", 0f, 15f);

		Long id1 = BasicFonctionnalitiesDelegate.doAddBuyerAndReturnId(buyer1);
		Long id2 = BasicFonctionnalitiesDelegate.doAddBuyerAndReturnId(buyer2);

		Buyer b1 = BasicFonctionnalitiesDelegate.doFindBuyerById(id1);
		Buyer b2 = BasicFonctionnalitiesDelegate.doFindBuyerById(id2);
		String CardNumber = "4242424242424242";
		String ExpirationDate = "0822";
		ProcessingDelegate.doChargeCreditCardForBuyer(new BigDecimal(300), b1, CardNumber, ExpirationDate);
		ProcessingDelegate.doChargeCreditCardForBuyer(new BigDecimal(100), b2, CardNumber, ExpirationDate);
	}
}