package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.CustomizedAds;

@Local
public interface CustomizedAdsServicesLocal {

	Boolean addCustomizedAds(CustomizedAds theCustomizedAds);

	Long addCustomizedAdsAndReturnId(CustomizedAds theCustomizedAds);

	CustomizedAds findCustomizedAdsById(Long theId);

	Boolean updateCustomizedAds(CustomizedAds theCustomizedAds);

	Boolean deleteCustomizedAds(CustomizedAds theCustomizedAds);

	Boolean deleteCustomizedAdsById(Long theId);

	List<CustomizedAds> findAllCustomizedAds();
}
