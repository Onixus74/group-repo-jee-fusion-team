package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;

@Remote
public interface ProductServicesRemote {

	Boolean addProduct(Product theProduct);

	Long addProductAndReturnId(Product theProduct);

	Product findProductById(Long theId);

	Boolean updateProduct(Product theProduct);

	Boolean deleteProduct(Product theProduct);

	Boolean deleteProductById(Long theId);

	List<Product> findAllProduct();

}
