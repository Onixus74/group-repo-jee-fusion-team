package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.SpecialPromotion;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SpecialPromotionServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.SpecialPromotionServicesRemote;

/**
 * Session Bean implementation class SpecialPromotionServices
 */
@Stateless
public class SpecialPromotionServices implements
		SpecialPromotionServicesRemote, SpecialPromotionServicesLocal {
	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public SpecialPromotionServices() {
	}

	@Override
	public Boolean addSpecialPromotion(SpecialPromotion theSpecialPromotion) {
		Boolean b = false;
		try {
			entityManager.persist(theSpecialPromotion);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public SpecialPromotion findSpecialPromotionById(Long theId) {
		return entityManager.find(SpecialPromotion.class, theId);
	}

	@Override
	public Boolean updateSpecialPromotion(SpecialPromotion theSpecialPromotion) {
		boolean b = false;
		try {
			entityManager.merge(theSpecialPromotion);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteSpecialPromotion(SpecialPromotion theSpecialPromotion) {
		Boolean b = false;
		try {
			theSpecialPromotion = findSpecialPromotionById(theSpecialPromotion
					.getSpecialPromotionId());
			entityManager.remove(theSpecialPromotion);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteSpecialPromotionById(Long theId) {
		Boolean b = false;
		try {
			entityManager.remove(findSpecialPromotionById(theId));
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SpecialPromotion> findAllSpecialPromotion() {
		Query query = entityManager
				.createQuery("select e from SpecialPromotion e ");
		return query.getResultList();
	}

	@Override
	public Long addSpecialPromotionAndReturnId(
			SpecialPromotion theSpecialPromotion) {

		try {
			entityManager.persist(theSpecialPromotion);
			entityManager.flush();
			return theSpecialPromotion.getSpecialPromotionId();
		} catch (Exception e) {
		}
		return null;	}

}
