package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SubcategoryServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.SubcategoryServicesRemote;

/**
 * Session Bean implementation class SubcategoryServices
 */
@Stateless
public class SubcategoryServices implements SubcategoryServicesRemote,
		SubcategoryServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public SubcategoryServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Boolean addSubcategory(Subcategory theSubcategory) {
		Boolean b = false;
		try {
			entityManager.persist(theSubcategory);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Subcategory findSubcategoryById(Long theId) {
		return entityManager.find(Subcategory.class, theId);
	}

	@Override
	public Boolean updateSubcategory(Subcategory theSubcategory) {
		boolean b = false;
		try {
			entityManager.merge(theSubcategory);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteSubcategory(Subcategory theSubcategory) {
		Boolean b = false;
		try {
			theSubcategory = findSubcategoryById(theSubcategory
					.getSubcategoryId());
			entityManager.remove(theSubcategory);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteSubcategoryById(Long theId) {
		Boolean b = false;
		try {
			entityManager.remove(findSubcategoryById(theId));
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Subcategory> findAllSubcategory() {
		Query query = entityManager.createQuery("select e from Subcategory e ");
		return query.getResultList();
	}

	@Override
	public Long addSubcategoryAndReturnId(Subcategory theSubcategory) {

		try {
			entityManager.persist(theSubcategory);
			entityManager.flush();
			return theSubcategory.getSubcategoryId();
		} catch (Exception e) {
		}
		return null;
	}

}
