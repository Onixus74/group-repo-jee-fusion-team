package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.BuyerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.BuyerServicesRemote;

/**
 * Session Bean implementation class BuyerServices
 */
@Stateless
public class BuyerServices implements BuyerServicesRemote, BuyerServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public BuyerServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Boolean addBuyer(Buyer theBuyer) {
		Boolean b = false;
		try {
			entityManager.persist(theBuyer);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Buyer findBuyerById(Long theId) {
		return entityManager.find(Buyer.class, theId);
	}

	@Override
	public Boolean updateBuyer(Buyer theBuyer) {
		boolean b = false;
		try {
			entityManager.merge(theBuyer);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteBuyer(Buyer theBuyer) {
		Boolean b = false;
		try {
			theBuyer = findBuyerById(theBuyer.getYouBayUserId());
			entityManager.remove(theBuyer);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteBuyerById(Long theId) {
		Boolean b = false;
		try {
			entityManager.remove(findBuyerById(theId));
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Buyer> findAllBuyer() {
		Query query = entityManager.createQuery("select e from Buyer e ");
		return query.getResultList();
	}

	@Override
	public Long addBuyerAndReturnId(Buyer theBuyer) {
		try {
			entityManager.persist(theBuyer);
			entityManager.flush();
			return theBuyer.getYouBayUserId();
		} catch (Exception e) {
		}
		return null;
	}

}
