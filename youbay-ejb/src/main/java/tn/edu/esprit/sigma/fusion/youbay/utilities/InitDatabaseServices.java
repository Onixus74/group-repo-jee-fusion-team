package tn.edu.esprit.sigma.fusion.youbay.utilities;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.AssistantItems;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Auction;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Category;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ClientType;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.CustomizedAds;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViews;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Manager;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ProductHistory;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.SpecialPromotion;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.AssistantItemsServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.AuctionServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.BuyerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.CategoryServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.CustomizedAdsServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.HistoryOfViewsServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ManagerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.OrderAndReviewServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ProductHistoryServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ProductServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SellerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SpecialPromotionServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SubcategoryServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.processing.EncryptionServices;
import tn.edu.esprit.sigma.fusion.youbay.processing.EncryptionServicesLocal;

/**
 * Session Bean implementation class InitDatabase
 */

@Stateless
// @Startup
@SuppressWarnings("deprecation")
public class InitDatabaseServices implements InitDatabaseServicesRemote {

	@PersistenceContext
	EntityManager em;
	@EJB
	AssistantItemsServicesLocal assistantItemsServicesLocal;
	@EJB
	AuctionServicesLocal auctionServicesLocal;
	@EJB
	BuyerServicesLocal buyerServicesLocal;
	@EJB
	CategoryServicesLocal categoryServicesLocal;
	@EJB
	CustomizedAdsServicesLocal customizedAdsServicesLocal;
	@EJB
	HistoryOfViewsServicesLocal historyOfViewsServicesLocal;
	@EJB
	ManagerServicesLocal managerServicesLocal;
	@EJB
	OrderAndReviewServicesLocal orderAndReviewServicesLocal;
	@EJB
	ProductHistoryServicesLocal productHistoryServicesLocal;
	@EJB
	ProductServicesLocal productServicesLocal;
	@EJB
	SellerServicesLocal sellerServicesLocal;
	@EJB
	SpecialPromotionServicesLocal specialPromotionServicesLocal;
	@EJB
	SubcategoryServicesLocal subcategoryServicesLocal;
	@EJB
	EncryptionServicesLocal encryptionServices;

	/**
	 * Default constructor.
	 * 
	 */

	public InitDatabaseServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void truncateAllTables() {

		int deletedCount = 0;
		// we need to keep the truncate order
		System.out.println("=====> BEGIN truncateAllTables BEGIN <==================");
		deletedCount = em.createQuery("DELETE FROM Auction").executeUpdate();
		System.out.println(
				"------------------------------> truncateAllTables : deleted " + deletedCount + " Auction <-------");

		deletedCount = em.createQuery("DELETE FROM AssistantItems").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " AssistantItems <-------");

		// ATTENTION : Search for a solution to reset AUTO_INCREMENT
		// em.createNativeQuery("ALTER TABLE t_assistantitems AUTO_INCREMENT =
		// 1").executeUpdate();

		deletedCount = em.createQuery("DELETE FROM SpecialPromotion").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " SpecialPromotion <-------");
		deletedCount = em.createQuery("DELETE FROM ProductHistory").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " ProductHistory <-------");

		deletedCount = em.createQuery("DELETE FROM CustomizedAds").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " CustomizedAds <-------");

		deletedCount = em.createQuery("DELETE FROM HistoryOfViews").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " HistoryOfViews <-------");

		deletedCount = em.createQuery("DELETE FROM OrderAndReview").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " OrderAndReview <-------");

		deletedCount = em.createQuery("DELETE FROM Product").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " Product <-------");

		deletedCount = em.createQuery("DELETE FROM Subcategory").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " Subcategory <-------");

		deletedCount = em.createQuery("DELETE FROM Category").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " Sellers <-------");

		System.out.println("==================> END truncateAllTables END <==================");

		// we need to keep the truncate order
		deletedCount = em.createQuery("DELETE FROM Manager").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " Managers <-------");

		deletedCount = em.createQuery("DELETE FROM Buyer").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " Buyers <-------");

		deletedCount = em.createQuery("DELETE FROM Seller").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " Sellers <-------");

	}

	@Override
	public void addUsers() {
		/*
		 * Adds 9 USER 3 manager + 3 seller + 3 buyer
		 */

		String buyerPassword = encryptionServices.encryptString("buyer");
		String sellerPassword = encryptionServices.encryptString("seller");
		String managerPassword = encryptionServices.encryptString("manager");

		System.out.println("-----> Adding Users <-----");

		Buyer buyera1 = new Buyer("Houssem", "Sabbegh", "sabbegh@gmail.com", "21623215434", new Date("17/05/1991"),
				"tunisia", true, false, "", "1 Purchase;", true, 0f, "18 rue ESPRIT", "", "Ghazela", 0f, 15f);
		buyera1.setPassword(buyerPassword);

		Buyer buyera2 = new Buyer("Yassine", "Latiri", "pro@latiri.com", "21623165189", new Date("17/05/1991"),
				"tunisia", true, false, "", "5 Purchases;", true, 0f, "18 rue ESPRIT", "", "Ghazela", 0f, 15f);
		buyera2.setPassword(buyerPassword);

		buyerServicesLocal.addBuyer(buyera1);
		buyerServicesLocal.addBuyer(buyera2);

		Seller seller2 = new Seller("Houssem", "Sabbegh", "seller2@seller.com", "+21623843694", new Date(2015, 05, 18),
				"TUNISIA", true, false, "", "emptybadges", 1000f, 10f, "A fake description made for tests.", 10f, false,
				"logo string");
		seller2.setPassword(sellerPassword);

		Seller seller3 = new Seller("Tarek", "Latiri", "seller3@seller.com", "+21628154534", new Date(2015, 05, 17),
				"TUNISIA", true, false, "", "emptybadges", 1000f, 10f, "A fake description made for tests.", 10f, false,
				"logo string");
		seller3.setPassword(sellerPassword);

		sellerServicesLocal.addSeller(seller2);
		sellerServicesLocal.addSeller(seller3);

		Manager manager1 = new Manager("manager1 first name", "manager1 last name", "manager1@manager.com",
				"+216238785434", new Date(2015, 05, 18), "Tunisia", true, false, "", true, true, true, true, true);
		manager1.setPassword(managerPassword);

		Manager manager2 = new Manager("manager2 first name", "manager2 last name", "manager2@manager.com", "+21627434",
				new Date(2015, 05, 18), "Tunisia", true, false, "", true, true, true, true, true);
		manager2.setPassword(managerPassword);

		Manager manager3 = new Manager("manager3 first name", "manager3 last name", "manager3@manager.com",
				"+21623884215434", new Date(2015, 05, 18), "Tunisia", true, false, "", true, true, true, true, true);
		manager3.setPassword(managerPassword);

		managerServicesLocal.addManager(manager1);
		managerServicesLocal.addManager(manager2);
		managerServicesLocal.addManager(manager3);

		Buyer buyer1 = new Buyer("buyer1 first name", "buyer 1 last name", "buyer1@buyer.com", "+216232134",
				new Date(2015 / 05 / 17), "TUNISIA", true, false, "", "no budget", true, 1547f, "lac II",
				"canada embassy", "tunisia", 54847954f, 41f);
		buyer1.setPassword(buyerPassword);

		Buyer buyer2 = new Buyer("buyer2 first name", "buyer 2 last name", "buyer2@buyer.com", "+21625434",
				new Date(2015 / 05 / 17), "TUNISIA", true, false, "", "no budget", true, 1547f, "lac II",
				"canada embacy", "tunisia", 54847954f, 41f);
		buyer2.setPassword(buyerPassword);

		Buyer buyer3 = new Buyer("buyer3 first name", "buyer 3 last name", "buyer3@buyer.com", "+21623215",
				new Date(2015 / 05 / 17), "TUNISIA", true, false, "", "no budget", true, 1547f, "lac II",
				"canada embacy", "tunisia", 54847954f, 41f);
		buyer3.setPassword(buyerPassword);

		buyerServicesLocal.addBuyer(buyer1);
		buyerServicesLocal.addBuyer(buyer2);
		buyerServicesLocal.addBuyer(buyer3);
	}

	@Override
	public void addAssistantItems() {
		AssistantItems assistantItems1 = new AssistantItems(10, "assistantItems1quetion1",
				"assistantItems1negativeanswer1", "assistantItems1affirmativeanswer1",
				"negativeAnswerQueryassistantItems1", "affirmativeAnswerQueryassistantItems1");

		AssistantItems assistantItems2 = new AssistantItems(10, "assistantItems2quetion1",
				"assistantItems2negativeanswer1", "assistantItems2affirmativeanswer1",
				"negativeAnswerQueryassistantItems2", "affirmativeAnswerQueryassistantItems2");

		AssistantItems assistantItems3 = new AssistantItems(10, "assistantItems3quetion1",
				"assistantItems3negativeanswer1", "assistantItems3affirmativeanswer1",
				"negativeAnswerQueryassistantItems3", "affirmativeAnswerQueryassistantItems3");

		assistantItemsServicesLocal.addAssistantItems(assistantItems1);
		assistantItemsServicesLocal.addAssistantItems(assistantItems2);
		assistantItemsServicesLocal.addAssistantItems(assistantItems3);

	}

	@Override
	public void addAuction() {
		Auction auction1 = new Auction(new Date(2015, 17, 05), new Date(2015, 17, 06), 10f);
		Auction auction2 = new Auction(new Date(2015, 17, 05), new Date(2015, 17, 06), 10f);
		Auction auction3 = new Auction(new Date(2015, 17, 05), new Date(2015, 17, 06), 10f);
		auctionServicesLocal.addAuction(auction1);
		auctionServicesLocal.addAuction(auction2);
		auctionServicesLocal.addAuction(auction3);
	}

	@Override
	public void addCategory() {
		Category category1 = new Category("Computers", 11);
		Category category2 = new Category("Electronics", 22);
		Category category3 = new Category("Accessories", 33);

		categoryServicesLocal.addCategory(category1);
		categoryServicesLocal.addCategory(category2);
		categoryServicesLocal.addCategory(category3);
	}

	@Override
	public void addCustomizedAds() {
		CustomizedAds customizedAds1 = new CustomizedAds(new Date(1991, 05, 17), new Date(2015, 05, 18), 254f,
				"customized ads 1 message", true, true);
		CustomizedAds customizedAds2 = new CustomizedAds(new Date(1991, 05, 17), new Date(2015, 05, 18), 254f,
				"customized ads 2 message", true, true);
		CustomizedAds customizedAds3 = new CustomizedAds(new Date(1991, 05, 17), new Date(2015, 05, 18), 254f,
				"customized ads 3 message", true, true);

		customizedAdsServicesLocal.addCustomizedAds(customizedAds1);
		customizedAdsServicesLocal.addCustomizedAds(customizedAds2);
		customizedAdsServicesLocal.addCustomizedAds(customizedAds3);

	}

	@Override
	public void addProduct() {

		addSubcategory();

		List<Subcategory> subcategories = subcategoryServicesLocal.findAllSubcategory();

		Subcategory computers_gaming = null, computers_desktop = null, computers_tablet = null,
				electronics_ereaders = null, electronics_hobby = null, electronics_tvs = null,
				accessories_backpack = null, accessories_keyboard = null, accessories_products = null;

		for (Subcategory c : subcategories) {

			if (c.getCategoryName().equals("Gaming Computer"))
				computers_gaming = c;
			if (c.getCategoryName().equals("Desktop Computer"))
				computers_desktop = c;
			if (c.getCategoryName().equals("Tablet"))
				computers_tablet = c;
			if (c.getCategoryName().equals("eReaders"))
				electronics_ereaders = c;
			if (c.getCategoryName().equals("Hobby Electronics"))
				electronics_hobby = c;
			if (c.getCategoryName().equals("TVs"))
				electronics_tvs = c;
			if (c.getCategoryName().equals("Computer Backpacks"))
				accessories_backpack = c;
			if (c.getCategoryName().equals("Mechanical Keyboards"))
				accessories_keyboard = c;
			if (c.getCategoryName().equals("Cleaning Products"))
				accessories_products = c;
		}

		Seller seller = new Seller("Yassine", "Latiri", "seller1@seller.com", "+21623256434", new Date(2015, 05, 18),
				"TUNISIA", true, false, "", "emptybadges", 1000f, 10f, "A fake description made for tests.", 10f, false,
				"logo string");
		seller.setPassword(encryptionServices.encryptString("seller"));
		sellerServicesLocal.addSeller(seller);

		if (computers_gaming != null) {
			Product product1 = new Product("Gaming Computer 1", "http://i.imgur.com/Zu4UvFl.png?2", "A Gaming Computer", 1000f, "", false, false, 6);
			Product product2 = new Product("Gaming Computer 2", "http://imgur.com/rcvpNea", "A Gaming Computer", 1500f, "", false, false, 5);

			product1.setSubcategory(computers_gaming);
			product2.setSubcategory(computers_gaming);
			
			product1.setSeller(seller);
			product2.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
		}

		if (computers_desktop != null) {
			Product product1 = new Product("Desktop Computer 1", "http://imgur.com/40NAHkA", "An Acer Desktop", 1000f, "", false, false, 5);
			Product product2 = new Product("Desktop Computer 2", "http://imgur.com/q6eC589", "HP Desktop Computer", 1000f, "", false, false, 5);

			product1.setSubcategory(computers_desktop);
			product2.setSubcategory(computers_desktop);
			
			product1.setSeller(seller);
			product2.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
		}

		if (computers_tablet != null) {
			Product product1 = new Product("Acer Tablet", "http://imgur.com/G6qKali", "Best Tablet ever !", 1000f, "", false, false, 5);
			Product product2 = new Product("Samsung Tablet", "http://imgur.com/FJc3CPr", "Android 4.5 1Gb RAM", 1000f, "", false, false, 5);

			product1.setSubcategory(computers_tablet);
			product2.setSubcategory(computers_tablet);
			
			product1.setSeller(seller);
			product2.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
		}

		if (electronics_ereaders != null) {
			Product product1 = new Product("Kindle eReader", "http://imgur.com/FJc3CPr", "Best paperwhite ereader", 100f, "", false, false, 6);
			Product product2 = new Product("Barnes & Nobles eReader", "http://imgur.com/FJc3CPr", "Color eReader", 210f, "", false, false, 7);

			product1.setSubcategory(electronics_ereaders);
			product2.setSubcategory(electronics_ereaders);
			
			product1.setSeller(seller);
			product2.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
		}

		if (electronics_hobby != null) {
			Product product1 = new Product("Amateur welding kit", "http://imgur.com/FJc3CPr", "For all your projects !", 180f, "", false, false,
					5);
			Product product2 = new Product("Ham radio", "http://imgur.com/FJc3CPr", "Amateur radio kit - receiver / transmitter", 320f, "",
					false, false, 5);

			product1.setSubcategory(electronics_hobby);
			product2.setSubcategory(electronics_hobby);
			
			product1.setSeller(seller);
			product2.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
		}

		if (electronics_tvs != null) {
			Product product1 = new Product("Samsung TV", "http://imgur.com/FJc3CPr", "Samsung TV", 500f, "", false, false, 5);
			Product product2 = new Product("Apple Smart TV", "http://imgur.com/FJc3CPr", "Apple", 400f, "", false, false, 5);

			product1.setSubcategory(electronics_tvs);
			product2.setSubcategory(electronics_tvs);
			
			product1.setSeller(seller);
			product2.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
		}

		if (accessories_backpack != null) {
			Product product1 = new Product("Black backpack", "http://imgur.com/FJc3CPr", "Black backpack", 1000f, "", false, false, 5);
			Product product2 = new Product("White backpack", "http://imgur.com/FJc3CPr", "White backpack", 1000f, "", false, false, 5);

			product1.setSubcategory(accessories_backpack);
			product2.setSubcategory(accessories_backpack);

			product1.setSeller(seller);
			product2.setSeller(seller);
			
			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
		}

		if (accessories_keyboard != null) {
			Product product1 = new Product("Razer Mechanical Keyboard", "http://imgur.com/FJc3CPr", "Razer Mechanical Keyboard", 1000f, "",
					false, false, 5);
			Product product2 = new Product("Logitech Keboard", "http://imgur.com/FJc3CPr", "Best Keyboard ever :)", 1000f, "", false, false, 5);

			product1.setSubcategory(accessories_keyboard);
			product2.setSubcategory(accessories_keyboard);

			product1.setSeller(seller);
			product2.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
		}

		if (accessories_products != null) {
			Product product1 = new Product("USB Fan", "http://imgur.com/FJc3CPr", "To cool your laptop.", 10f, "", false, false, 5);
			Product product2 = new Product("Thermal grease", "http://imgur.com/FJc3CPr", "HP Laptop", 1000f, "", false, false, 5);

			product1.setSubcategory(accessories_products);
			product2.setSubcategory(accessories_products);

			product1.setSeller(seller);
			product2.setSeller(seller);			
			
			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
		}

	}

	@Override
	public void addProductHistory() {
		ProductHistory productHistory1 = new ProductHistory(new Date(1478, 05, 17), "product history1 name",
				"product history1 image URL", "short description", "productMainDescriptionHistory", 10f,
				"subcategoryAdditionalValuesHistory", 10);
		ProductHistory productHistory2 = new ProductHistory(new Date(1478, 05, 17), "product history2 name",
				"product history2 image URL", "short description", "productMainDescriptionHistory", 10f,
				"subcategoryAdditionalValuesHistory", 10);
		ProductHistory productHistory3 = new ProductHistory(new Date(1478, 05, 17), "product history3 name",
				"product history3 image URL", "short description", "productMainDescriptionHistory", 10f,
				"subcategoryAdditionalValuesHistory", 10);
		productHistoryServicesLocal.addProductHistory(productHistory1);
		productHistoryServicesLocal.addProductHistory(productHistory2);
		productHistoryServicesLocal.addProductHistory(productHistory3);

	}

	@Override
	public void addSpecialPromotion() {
		SpecialPromotion promotion1 = new SpecialPromotion(new Date(2014, 05, 17), new Date(2014, 05, 17), false,
				"deal1 ShortDescription", "deal1 Description", 5f);
		SpecialPromotion promotion2 = new SpecialPromotion(new Date(2014, 05, 17), new Date(2014, 05, 17), false,
				"deal2 ShortDescription", "deal2 Description", 5f);
		SpecialPromotion promotion3 = new SpecialPromotion(new Date(2014, 05, 17), new Date(2014, 05, 17), false,
				"deal3 ShortDescription", "deal3 Description", 5f);
		specialPromotionServicesLocal.addSpecialPromotion(promotion1);
		specialPromotionServicesLocal.addSpecialPromotion(promotion2);
		specialPromotionServicesLocal.addSpecialPromotion(promotion3);

	}

	@Override
	public void addSubcategory() {

		addCategory();

		List<Category> categories = categoryServicesLocal.findAllCategory();

		Category computers = null, electronics = null, accessories = null;

		for (Category c : categories) {
			if (c.getCategoryName().equals("Computers"))
				computers = c;

			if (c.getCategoryName().equals("Electronics"))
				electronics = c;

			if (c.getCategoryName().equals("Accessories"))
				accessories = c;
		}

		if (computers != null) {
			Subcategory subcategory1 = new Subcategory("Gaming Computer", 10, "", true, "avatar image");
			Subcategory subcategory2 = new Subcategory("Desktop Computer", 20, "", true, "avatar image");
			Subcategory subcategory3 = new Subcategory("Tablet", 30, "", true, "avatar image");

			subcategory1.setCategory(computers);
			subcategory2.setCategory(computers);
			subcategory3.setCategory(computers);

			subcategoryServicesLocal.addSubcategory(subcategory1);
			subcategoryServicesLocal.addSubcategory(subcategory2);
			subcategoryServicesLocal.addSubcategory(subcategory3);
		}

		if (electronics != null) {
			Subcategory subcategory4 = new Subcategory("eReaders", 10, "", true, "avatar image");
			Subcategory subcategory5 = new Subcategory("Hobby Electronics", 20, "", true, "avatar image");
			Subcategory subcategory6 = new Subcategory("TVs", 30, "", true, "avatar image");

			subcategory4.setCategory(electronics);
			subcategory5.setCategory(electronics);
			subcategory6.setCategory(electronics);

			subcategoryServicesLocal.addSubcategory(subcategory4);
			subcategoryServicesLocal.addSubcategory(subcategory5);
			subcategoryServicesLocal.addSubcategory(subcategory6);
		}

		if (accessories != null) {
			Subcategory subcategory7 = new Subcategory("Computer Backpacks", 10, "", true, "avatar image");
			Subcategory subcategory8 = new Subcategory("Mechanical Keyboards", 20, "", true, "avatar image");
			Subcategory subcategory9 = new Subcategory("Cleaning Products", 30, "", true, "avatar image");

			subcategory7.setCategory(accessories);
			subcategory8.setCategory(accessories);
			subcategory9.setCategory(accessories);

			subcategoryServicesLocal.addSubcategory(subcategory7);
			subcategoryServicesLocal.addSubcategory(subcategory8);
			subcategoryServicesLocal.addSubcategory(subcategory9);
		}
	}

	@Override
	public void addHistoryOfViews() {
		/* Always run this method AFTER running addUsers and addProdutcs ! */

		List<Buyer> listBuyers = buyerServicesLocal.findAllBuyer();
		List<Product> listProducts = productServicesLocal.findAllProduct();

		HistoryOfViews historyItem1 = new HistoryOfViews("Added automatically", ClientType.other, listBuyers.get(0),
				listProducts.get(0), new Date());
		HistoryOfViews historyItem2 = new HistoryOfViews("Added automatically", ClientType.other, listBuyers.get(0),
				listProducts.get(1), new Date());
		HistoryOfViews historyItem3 = new HistoryOfViews("Added automatically", ClientType.other, listBuyers.get(1),
				listProducts.get(0), new Date());

		historyOfViewsServicesLocal.addHistoryOfViews(historyItem1);
		historyOfViewsServicesLocal.addHistoryOfViews(historyItem2);
		historyOfViewsServicesLocal.addHistoryOfViews(historyItem3);

	}

	@Override
	public void addOrderAndReview() {
		/* Always run this method AFTER running addUsers and addProdutcs ! */

		List<Buyer> listBuyers = buyerServicesLocal.findAllBuyer();
		List<Product> listProducts = productServicesLocal.findAllProduct();

		OrderAndReview order1 = new OrderAndReview(15f, "Please package the items carefuly !", false, false, new Date(),
				false, false, null, null, null, listBuyers.get(0), listProducts.get(0));

		OrderAndReview order2 = new OrderAndReview(15f, "Please package the items carefuly !", false, false, new Date(),
				false, false, null, null, null, listBuyers.get(1), listProducts.get(1));

		orderAndReviewServicesLocal.addOrderAndReview(order1);
		orderAndReviewServicesLocal.addOrderAndReview(order2);
	}

	@Override
	// @PostConstruct
	public void populateDatabase() {

		System.out.println("==================> INTITIALISING DATABASE ! <==================");

		/* Always run these methods FIRST */
		addUsers();
		addAssistantItems();
		addAuction();
		addCustomizedAds();
		addProduct();
		addProductHistory();
		addSpecialPromotion();

		/* Always run these methods LAST */
		addHistoryOfViews();
		addOrderAndReview();

	}

}
