package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Auction;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.AuctionServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.AuctionServicesRemote;

/**
 * Session Bean implementation class AuctionServices
 */
@Stateless
public class AuctionServices implements AuctionServicesRemote,
		AuctionServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public AuctionServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Boolean addAuction(Auction theAuction) {
		Boolean b = false;
		try {
			entityManager.persist(theAuction);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Auction findAuctionById(Long theId) {
		return entityManager.find(Auction.class, theId);
	}

	@Override
	public Boolean updateAuction(Auction theAuction) {
		boolean b = false;
		try {
			entityManager.merge(theAuction);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteAuction(Auction theAuction) {
		Boolean b = false;
		try {
			theAuction = findAuctionById(theAuction.getAuctionId());
			entityManager.remove(theAuction);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteAuctionById(Long theId) {
		Boolean b = false;
		try {
			entityManager.remove(findAuctionById(theId));
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Auction> findAllAuction() {
		Query query = entityManager.createQuery("select e from Auction e ");
		return query.getResultList();
	}

	@Override
	public Long addAuctionAndReturnId(Auction theAuction) {

		try {
			entityManager.persist(theAuction);
			entityManager.flush();
			return theAuction.getAuctionId();
		} catch (Exception e) {
		}
		return null;	}

}
