package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.CustomizedAds;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.CustomizedAdsServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.CustomizedAdsServicesRemote;

/**
 * Session Bean implementation class CustomizedAdsServices
 */
@Stateless
public class CustomizedAdsServices implements CustomizedAdsServicesRemote,
		CustomizedAdsServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public CustomizedAdsServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Boolean addCustomizedAds(CustomizedAds theCustomizedAds) {
		Boolean b = false;
		try {
			entityManager.persist(theCustomizedAds);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	

	@Override
	public CustomizedAds findCustomizedAdsById(Long theId) {
		return entityManager.find(CustomizedAds.class, theId);
	}

	@Override
	public Boolean updateCustomizedAds(CustomizedAds theCustomizedAds) {
		boolean b = false;
		try {
			entityManager.merge(theCustomizedAds);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteCustomizedAds(CustomizedAds theCustomizedAds) {
		Boolean b = false;
		try {
			theCustomizedAds = findCustomizedAdsById(theCustomizedAds
					.getCustomizedAdsId());
			entityManager.remove(theCustomizedAds);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteCustomizedAdsById(Long theId) {
		Boolean b = false;
		try {
			entityManager.remove(findCustomizedAdsById(theId));
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CustomizedAds> findAllCustomizedAds() {
		Query query = entityManager.createQuery("select e from CustomizedAds e ");
		return query.getResultList();
	}

	@Override
	public Long addCustomizedAdsAndReturnId(CustomizedAds theCustomizedAds) {

		try {
			entityManager.persist(theCustomizedAds);
			entityManager.flush();
			return theCustomizedAds.getCustomizedAdsId();
		} catch (Exception e) {
		}
		return null;	}
}
