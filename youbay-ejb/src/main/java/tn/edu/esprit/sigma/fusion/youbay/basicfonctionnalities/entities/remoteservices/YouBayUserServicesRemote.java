package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.YouBayUser;

@Remote
public interface YouBayUserServicesRemote {

	YouBayUser findYouBayUserByEmail(String email);
	YouBayUser findYouBayUserByEmail(String email, String password);
}
