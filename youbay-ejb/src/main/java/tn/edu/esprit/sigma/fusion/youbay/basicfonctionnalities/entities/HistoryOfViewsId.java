package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;

@Embeddable
public class HistoryOfViewsId implements Serializable {

	private Date theDate;
	private Long ProductId;
	private Long BuyerId;

	private static final long serialVersionUID = 1L;

	public HistoryOfViewsId() {
		super();
	}

	public HistoryOfViewsId(Long productId, Long buyerId, Date theDate) {
		super();
		this.ProductId = productId;
		this.BuyerId = buyerId;
		this.theDate = theDate;
	}

	public Date getTheDate() {
		return theDate;
	}


	public void setTheDate(Date theDate) {
		this.theDate = theDate;
	}

	public Long getProductId() {
		return this.ProductId;
	}

	public void setProductId(Long ProductId) {
		this.ProductId = ProductId;
	}

	public Long getBuyerId() {
		return this.BuyerId;
	}

	public void setBuyerId(Long BuyerId) {
		this.BuyerId = BuyerId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((BuyerId == null) ? 0 : BuyerId.hashCode());
		result = prime * result
				+ ((ProductId == null) ? 0 : ProductId.hashCode());
		result = prime * result + ((theDate == null) ? 0 : theDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoryOfViewsId other = (HistoryOfViewsId) obj;
		if (BuyerId == null) {
			if (other.BuyerId != null)
				return false;
		} else if (!BuyerId.equals(other.BuyerId))
			return false;
		if (ProductId == null) {
			if (other.ProductId != null)
				return false;
		} else if (!ProductId.equals(other.ProductId))
			return false;
		if (theDate == null) {
			if (other.theDate != null)
				return false;
		} else if (!theDate.equals(other.theDate))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "HistoryOfViewsId [theDate=" + theDate + ", ProductId="
				+ ProductId + ", BuyerId=" + BuyerId + "]";
	}
	

}