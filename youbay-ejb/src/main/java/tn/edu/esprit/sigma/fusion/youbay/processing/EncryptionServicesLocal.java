package tn.edu.esprit.sigma.fusion.youbay.processing;

import javax.ejb.Local;

@Local
public interface EncryptionServicesLocal {
	public String encryptString(String string);
	public String byteToHex(final byte[] hash);
}
