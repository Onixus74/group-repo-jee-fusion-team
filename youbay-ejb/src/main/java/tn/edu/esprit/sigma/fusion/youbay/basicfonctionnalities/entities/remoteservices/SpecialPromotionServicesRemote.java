package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.SpecialPromotion;

@Remote
public interface SpecialPromotionServicesRemote {

	Boolean addSpecialPromotion(SpecialPromotion theSpecialPromotion);

	Long addSpecialPromotionAndReturnId(SpecialPromotion theSpecialPromotion);

	SpecialPromotion findSpecialPromotionById(Long theId);

	Boolean updateSpecialPromotion(SpecialPromotion theSpecialPromotion);

	Boolean deleteSpecialPromotion(SpecialPromotion theSpecialPromotion);

	Boolean deleteSpecialPromotionById(Long theId);

	List<SpecialPromotion> findAllSpecialPromotion();

}
