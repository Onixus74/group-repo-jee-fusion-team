package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;

@Local
public interface SellerServicesLocal {

	Boolean addSeller(Seller theSeller);
	
	Long addSellerAndReturnId(Seller theSeller);

	Seller findSellerById(Long theId);

	Boolean updateSeller(Seller theSeller);

	Boolean deleteSeller(Seller theSeller);

	Boolean deleteSellerById(Long theId);

	List<Seller> findAllSeller();

}
