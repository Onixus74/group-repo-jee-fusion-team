package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SellerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.SellerServicesRemote;

/**
 * Session Bean implementation class SellerServices
 */
@Stateless
public class SellerServices implements SellerServicesRemote,
		SellerServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public SellerServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Boolean addSeller(Seller theSeller) {
		Boolean b = false;
		try {
			entityManager.persist(theSeller);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Seller findSellerById(Long theId) {
		return entityManager.find(Seller.class, theId);
	}

	@Override
	public Boolean updateSeller(Seller theSeller) {
		boolean b = false;
		try {
			entityManager.merge(theSeller);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteSeller(Seller theSeller) {
		Boolean b = false;
		try {
			theSeller = findSellerById(theSeller.getYouBayUserId());
			entityManager.remove(theSeller);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteSellerById(Long theId) {
		Boolean b = false;
		try {
			entityManager.remove(findSellerById(theId));
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Seller> findAllSeller() {
		Query query = entityManager.createQuery("select e from Seller e ");
		return query.getResultList();
	}

	@Override
	public Long addSellerAndReturnId(Seller theSeller) {

		try {
			entityManager.persist(theSeller);
			entityManager.flush();
			return theSeller.getYouBayUserId();
		} catch (Exception e) {
		}
		return null;	}

}
