package tn.edu.esprit.sigma.fusion.youbaywar.managedbeans.utilities;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.NotNull;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Manager;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.YouBayUser;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.YouBayUserServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.processing.EncryptionServicesLocal;

@ManagedBean
@RequestScoped
public class Authentification {
	private String email;
	private String password;

	@ManagedProperty("#{identityBean}")
	IdentityBean identityBean;

	@EJB
	private YouBayUserServicesLocal youBayUserServicesLocal;
	@EJB
	private EncryptionServicesLocal encryptionServicesLocal;

	public String doLogin() {
		String navigateTo = null;

		YouBayUser youBayUserfound = youBayUserServicesLocal.findYouBayUserByEmail(email);

		if (youBayUserfound == null) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "login incorrect", null));
		} else if (!youBayUserfound.getPassword().equals(encryptionServicesLocal.encryptString(password))) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Password incorrecte", null));
		}

		else {

			if (!youBayUserfound.getIsActive()) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Suspended account please contact admin", null));
			} else {
				identityBean.setYouBayUserfound(youBayUserfound);
				if (youBayUserfound instanceof Manager) {
					navigateTo = "/manager/home?faces-redirect=true";
				} else if (youBayUserfound instanceof Seller) {
					navigateTo = "/seller/home?faces-redirect=true";
				} else if (youBayUserfound instanceof Buyer) {
					navigateTo = "/buyer/home?faces-redirect=true";
				}

			}

		}

		return navigateTo;
	}

	public String doLogout() {
		String navigatTo = null;
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
		navigatTo = "/login?faces-redirect=true";
		return navigatTo;
	}

	public void setIdentityBean(IdentityBean identityBean) {
		this.identityBean = identityBean;
	}

	@NotNull(message = "Email is required")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}